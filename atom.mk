LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := mbedtls
LOCAL_DESCRIPTION := SSL library

LOCAL_CFLAGS := -I$(PWD)/external/mbedtls/configs/ -I$(PWD)/external/mbedtls/include/ -DMBEDTLS_CONFIG_FILE='<config-mini-tls1_1.h>'

LOCAL_CMAKE_CONFIGURE_ARGS := -DENABLE_TESTING=Off -DENABLE_PROGRAMS=Off

LOCAL_EXPORT_LDLIBS = -l:libmbedtls.a -l:libmbedx509.a -l:libmbedcrypto.a

include $(BUILD_CMAKE)

